name             'hack'
version          '0.2.0'
description      'Installs whatever I want to hack'
license          'Apache 2.0'
maintainer       'Marty Pauley'
maintainer_email 'marty+chef@martian.org'
supports         'ubuntu'
depends "apt"
