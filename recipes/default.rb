package "git"
package "make"
package "exuberant-ctags"

node[:hack].each do |x|
    include_recipe "#{cookbook_name}::#{x}"
end

