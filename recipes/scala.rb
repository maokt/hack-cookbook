%w/ scala scala-doc /.each do |pkg|
    package pkg do
        action :upgrade
    end
end

apt_repository "sbt" do
    uri "http://dl.bintray.com/sbt/debian"
    components [ "/" ]
end

package "sbt" do
    options "--allow-unauthenticated"
end
