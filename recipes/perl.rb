package "perl"
package "perl-doc"
package "libscalar-list-utils-perl"
package "cpanminus"

# we want web
package "libplack-perl"
package "starman"

# I don't like OO, but...
package "libmoo-perl"
